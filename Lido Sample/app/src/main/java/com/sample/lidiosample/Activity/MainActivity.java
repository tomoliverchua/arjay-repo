package com.sample.lidiosample.Activity;



import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.sample.lidiosample.Adapter.NavigationMenuAdapter;
import com.sample.lidiosample.Model.MenuObject;
import com.sample.lidiosample.Model.Nav3ModelClass;
import com.sample.lidiosample.R;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageClickListener;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    List<MenuObject> menuObjectList = new ArrayList<>();

    // List if images to display in the carousel
    public int[] mImages = new int[]{
            R.drawable.lechon_macao,
            R.drawable.lomi,
            R.drawable.shantung,
            R.drawable.manchurian_wings,
            R.drawable.pugon,
            R.drawable.lido_building
    };

    Nav3ModelClass[] nav3ModelClasses = {
            new Nav3ModelClass(R.drawable.ic_dehaze_black_24dp, "PROFILE", ""),
            new Nav3ModelClass(R.drawable.ic_dehaze_black_24dp, "ACTIVITY", ""),
            new Nav3ModelClass(R.drawable.ic_dehaze_black_24dp, "LEGENDS", "10"),
            new Nav3ModelClass(R.drawable.ic_dehaze_black_24dp, "LOGOUT", "")

    };

    CarouselView carouselView;

    Context context = this;

    private ActionBarDrawerToggle actionBarDrawerToggle;
    private DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar;
    ImageView menu;

    NavigationMenuAdapter navigationMenuAdapter;
    RecyclerView recycleview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigationmenu5);
        Initvar();
        // Get the instance of the carousel view from the layout

        // Set Carousel view max length
        carouselView.setPageCount(mImages.length);
        // Set image listener (track which page is the carousel view and set its image)
        carouselView.setImageListener(new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                imageView.setImageResource(mImages[position]);
            }
        });
        // Set On click Listener for the current page
        carouselView.setImageClickListener(new ImageClickListener() {
            @Override
            public void onClick(int position) {
                Toast.makeText(MainActivity.this,
                        "Current Position: " + position,
                        Toast.LENGTH_SHORT).show();
            }
        });

        navigationMenuAdapter = new NavigationMenuAdapter(context, nav3ModelClasses);
        recycleview.setLayoutManager(new LinearLayoutManager(context));
        recycleview.setAdapter(navigationMenuAdapter);


        setToolbar();

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);

        invalidateOptionsMenu();

        customDialog();
    }

    private void Initvar(){
        carouselView = findViewById(R.id.carouse_view1);
        drawer = findViewById(R.id.drawer_layout);
        navigationView =  findViewById(R.id.nav_view1);
        menu = findViewById(R.id.navigation_menu);
        recycleview =  findViewById(R.id.recycleview);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {

            drawer.closeDrawer(Gravity.LEFT); //OPEN Nav Drawer!
        } else {
            finish();
        }

    }

    private void setToolbar() {
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle("");

        toolbar.findViewById(R.id.navigation_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Click", "keryu");

                if (drawer.isDrawerOpen(navigationView)) {
                    drawer.closeDrawer(navigationView);
                } else {
                    drawer.openDrawer(navigationView);
                }
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void customDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_advisory, null);
        dialogBuilder.setView(dialogView);



//        EditText editText = (EditText) dialogView.findViewById(R.id.label_field);
//        editText.setText("test label");
        AlertDialog alertDialog = dialogBuilder.create();

        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 90);
        alertDialog.getWindow().setBackgroundDrawable(inset);
        alertDialog.show();
    }

}
