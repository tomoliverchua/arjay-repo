package com.sample.lidiosample.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sample.lidiosample.Model.MenuObject;

import java.util.List;

public class navmenuAdapter extends BaseAdapter {

    List<MenuObject> menuObjectList;
    Context context;

    public navmenuAdapter(Context context, List<MenuObject> menuObjects) {
        this.menuObjectList = menuObjects;
        this.context = context;
    }

    @Override
    public int getCount() {
        return menuObjectList.size();
    }

    @Override
    public Object getItem(int position) {
        return menuObjectList.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {


        return null;
    }
}
