package com.sample.lidiosample.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sample.lidiosample.Fragments.BurgerFragment;
import com.sample.lidiosample.Fragments.FirstFragment;
import com.sample.lidiosample.Fragments.FourthFragment;
import com.sample.lidiosample.Fragments.SecondFragment;
import com.sample.lidiosample.R;


public class ViewPageAdapter extends FragmentPagerAdapter {
    private Context context;

    private final String[] mTabsTitle = {"Home", "Menu", "Store locator","Coupon","Coupon","Coupon","Coupon"};
    private int mTabsIcons[];


    public ViewPageAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    public View getTabView(int position) {
        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View view = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        ImageView icon = view.findViewById(R.id.icon);
        TextView title = view.findViewById(R.id.title);
        title.setText(mTabsTitle[position]);
        Log.d("fetchi",mTabsTitle[position].toString() + " " + getCount()) ;
        return view;
    }

    @Override
    public Fragment getItem(int pos) {
        switch (pos) {
            case 0:
                return FirstFragment.newInstance(1);
            case 1:
                return FirstFragment.newInstance(2);
            case 2:
                return FirstFragment.newInstance(3);
            case 3:
                return FirstFragment.newInstance(4);
            case 4:
                return FirstFragment.newInstance(5);
            case 5:
                return FirstFragment.newInstance(6);
            case 6:
                return FirstFragment.newInstance(7);
        }
        return null;
    }

    @Override
    public int getCount() {
        int PAGE_COUNT = 7;
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabsTitle[position];
    }
}
