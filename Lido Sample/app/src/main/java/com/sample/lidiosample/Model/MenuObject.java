package com.sample.lidiosample.Model;

public class MenuObject {
    private String list;
    private String number;

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


}
