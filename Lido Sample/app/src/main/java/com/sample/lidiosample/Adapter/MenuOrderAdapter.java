package com.sample.lidiosample.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sample.lidiosample.Model.BookOrder;
import com.sample.lidiosample.R;

import java.util.List;

import customfonts.MyTextView_Roboto_Bold;


/**
 * Created by World of UI/UX on 01/04/2019.
 */

public class MenuOrderAdapter extends RecyclerView.Adapter<MenuOrderAdapter.MyViewHolder> {

     Context context;
    private List<BookOrder> OfferList;
    private int myPos = 0;

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img_menu_items;
        TextView tv_food_title;
        TextView tv_price;
        TextView btn_view;

        MyViewHolder(View view) {
            super(view);

            img_menu_items = view.findViewById(R.id.img_menu);
            tv_food_title =  view.findViewById(R.id.tv_item_name);
            tv_price =  view.findViewById(R.id.tv_item_price);
            btn_view = view.findViewById(R.id.btn_view);
        }
    }

    public MenuOrderAdapter(Context context, List<BookOrder> offerList) {
        this.OfferList = offerList;
        this.context = context;
    }

    @Override
    public MenuOrderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lido_menu_items, parent, false);
        return new MenuOrderAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final BookOrder lists = OfferList.get(position);
        holder.img_menu_items.setImageResource(lists.getImage());
        holder.tv_food_title.setText(lists.getTitle());
        holder.tv_price.setText(lists.getPrice());

//        if (myPos == position){
//            holder.title.setTextColor(Color.parseColor("#FF3B30"));
//            holder.linear.setBackgroundResource(R.drawable.ic_selector_1);
//        }else {
//            holder.title.setTextColor(Color.parseColor("#ffffff"));
//            holder.linear.setBackgroundResource(R.drawable.ic_selector_2);
//        }



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myPos = position;
                notifyDataSetChanged();

                if (myPos == 0){
//                    Intent intent = new Intent (view.getContext(), Scan.class);
//                    view.getContext().startActivity(intent);
                }

                if (myPos == 1){
//                    Intent intent = new Intent (view.getContext(), Profile.class);
//                    view.getContext().startActivity(intent);
                }

            }


        });

    }

    @Override
    public int getItemCount() {
        return OfferList.size();
    }

}


