package com.sample.lidiosample.Activity;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sample.lidiosample.Adapter.ViewPageAdapter;
import com.sample.lidiosample.R;

public class StoreMenu extends AppCompatActivity implements View.OnClickListener {

    TabLayout mTabLayout;
    private  int PAGENO = 1;
    TextView tv_arrow_back, tv_arrow_forward;

    private int[] mTabsIcons = {
            R.drawable.ic_mcdo,
            R.drawable.ic_burger,
            R.drawable.ic_pin,
            R.drawable.ic_pin,
            R.drawable.ic_pin,
            R.drawable.ic_pin,
            R.drawable.ic_coupon};

    private String txt[]={"Home","Menu","Store Locator","Coupon","Coupon","Coupon","Coupon"};

    Context context = this;
    ViewPageAdapter pagerAdapter;
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_menu);
        InitVar();
        // Setup the viewPager

        pagerAdapter = new ViewPageAdapter(getSupportFragmentManager(),context);
        if (viewPager != null)
            viewPager.setAdapter(pagerAdapter);

        mTabLayout = findViewById(R.id.tab_layout);
        if (mTabLayout != null) {
            mTabLayout.setupWithViewPager(viewPager);

            for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = mTabLayout.getTabAt(i);
                if (tab != null)
                    tab.setCustomView(pagerAdapter.getTabView(i));
            }
            mTabLayout.getTabAt(0).getCustomView().setSelected(true);
        }



        tv_arrow_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        tv_arrow_back.setOnClickListener(this);
        tv_arrow_forward.setOnClickListener(this);

    }

    private void InitVar(){
        tv_arrow_back = findViewById(R.id.tv_back_items);
        tv_arrow_forward = findViewById(R.id.tv_forward_items);
        viewPager = findViewById(R.id.view_pager);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_back_items:
                tabArrowBack();
                break;
            case R.id.tv_forward_items:
                tabArrowForward();
                break;

        }
    }

    private void tabArrowForward(){
        if(PAGENO != 6){
            PAGENO += 1;
            viewPager.setCurrentItem(PAGENO);
            Toast.makeText(context, "" + PAGENO, Toast.LENGTH_SHORT).show();
        }
        else if(PAGENO > 6){
            Toast.makeText(context, " Exceed" , Toast.LENGTH_SHORT).show();
        }
    }

    private void tabArrowBack(){
        if(PAGENO != 0){
            PAGENO -= 1;
            viewPager.setCurrentItem(PAGENO);
            Toast.makeText(context, "" + PAGENO, Toast.LENGTH_SHORT).show();
        }
        else if(PAGENO < 1){
            Toast.makeText(context, " Zero" , Toast.LENGTH_SHORT).show();
        }
    }
}
