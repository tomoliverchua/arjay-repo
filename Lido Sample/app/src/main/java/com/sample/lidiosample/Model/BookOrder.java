package com.sample.lidiosample.Model;

/**
 * Created by World of UI/UX on 01/04/2019.
 */

public class BookOrder {

    Integer image;
    String title;
    String price;

    public BookOrder(Integer image, String title,String price) {
        this.image = image;
        this.title = title;
        this.price = price;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

}
